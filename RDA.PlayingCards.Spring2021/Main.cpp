
// Ryan Appel
// Playing Cards

//PART 2 of assignment created by Devon Norman.

#include <iostream>
#include <conio.h>

using namespace std;

// enums for rank & suit
enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven,
	Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{

	//Suit Enum Key: Spades = 0, Hearts = 1, Clubs = 2, Diamonds = 3
	Spades, Hearts, Clubs, Diamonds
};


// struct for card
struct Card
{
	Rank rank;
	Suit suit;
};


void PrintCard(Card c) 
{

	//Prints out "The "
	cout << "The ";
	
	//Detects specific words to change the enum into. (Needs 2-10 in words)
	switch (c.rank)
	{
	//c.rank = 2
	case 2:
		cout << "Two";
		break;

	//c.rank = 3
	case 3:
		cout << "Three";
		break;

	//c.rank = 4
	case 4:
		cout << "Four";
		break;

	//c.rank = 5
	case 5:
		cout << "Five";
		break;

	//c.rank = 6
	case 6:
		cout << "Six";
		break;

	//c.rank = 7
	case 7:
		cout << "Seven";
		break;

	//c.rank = 8
	case 8:
		cout << "Eight";
		break;

	//c.rank = 9
	case 9:
		cout << "Nine";
		break;

	//c.rank = 10
	case 10:
		cout << "Ten";
		break;

	//c.rank = 11
	case 11:
		cout << "Jack";
		break;

	//c.rank = 12
	case 12:
		cout << "Queen";
		break;

	//c.rank = 13
	case 13:
		cout << "King";
		break;

	//c.rank = 14
	case 14:
		cout << "Ace";
		break;
	}

	// Prints the word "of" between the displayed c.rank and c.suit
	cout << " of ";
	

	//Switch case to determine the suit of the card.
	//Suit Enum Key: Spades = 0, Hearts = 1, Clubs = 2, Diamonds = 3
	switch (c.suit) 
	{

		//c.suit = 0 (spades)
		case 0:
			cout << "Spades\n"; // "\n" added after each case to end the line
			break;

		//c.suit = 1 (hearts)
		case 1:
			cout << "Hearts\n";
			break;

		//c.suit = 2 (clubs)
		case 2:
			cout << "Clubs\n";
			break;

		//c.suit = 3 (diamonds)	
		case 3:
			cout << "Diamonds\n";
			break;
	}



}


Card HighCard(Card card1, Card card2)
{
	//Prints card1 is card1 is greater than card2
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	//Prints card 2 if card1 is less than card2
	else if (card1.rank < card2.rank)
	{
		return card2;
	}
	//If both ranks are tied, the tie is broken depending on what suit wins out. (
	else
	{
		//Runs on a less-than is winner system for spades to be the highest, which has the lowest value. 0 is highest (spades), 3 is lowest (
		if (card1.suit < card2.suit)
		{
			return card1;
		}
		else 
		{
			return card2;
		}
	}
	
	//Shouldn't print anything out
}



int main()
{
	Card a;
	a.rank = Jack;
	a.suit = Hearts;
	PrintCard(a); //Prints Card of C

	Card b;
	b.rank = Ace;
	b.suit = Spades;
	PrintCard(b); //Prints Card of C


	Card c;
	c.rank = Ace;
	c.suit = Diamonds;
	PrintCard(c); //Prints Card of C

	Card d;
	d.rank = Three;
	d.suit = Diamonds;
	PrintCard(d); //Prints Card of C


	//This code will make whitespace between the info of the four cards, than display the highcard between card c and d.
	cout << "\n";
	cout << "----Highest Ranking Card----\n";

	PrintCard(HighCard(c, d));


	(void)_getch();
	return 0;
}
